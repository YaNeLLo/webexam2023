import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import PatchView from '../views/PatchView.vue'
import MobView from '../views/MobView.vue'

const routes = [
  {path: '/',component: HomeView},
  {path: '/patch',component: PatchView},
  {path: '/patch/:id',component: MobView}
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
